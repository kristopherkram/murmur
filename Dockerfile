FROM golang AS builder
WORKDIR /go/src/gitlab.com/kristopherkram/murmur-container
COPY go.mod .
COPY main.go .
#RUN go get -u gopkg.in/spf13/viper.v1.7.1
#RUN go get -u gopkg.in/ini.v1
RUN go get gitlab.com/kristopherkram/murmur
RUN go version
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o config-writer
#RUN go build .

FROM alpine AS murmur
RUN apk add wget tar
WORKDIR /tmp
RUN wget https://github.com/mumble-voip/mumble/releases/download/1.3.4/murmur-static_x86-1.3.4.tar.bz2 -O murmur-static_x86.tar.bz2
RUN ls
RUN tar -xf murmur-static_x86.tar.bz2

FROM scratch
COPY --from=builder /go/src/gitlab.com/kristopherkram/murmur-container/config-writer .
COPY --from=murmur /tmp/murmur-static_x86-1.3.4/murmur.x86 .
COPY --from=murmur /tmp/murmur-static_x86-1.3.4/LICENSE .
ENTRYPOINT ["/config-writer"]
EXPOSE 64738/tcp
EXPOSE 64738/udp