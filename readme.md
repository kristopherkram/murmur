Docker container for [murmur](https://www.mumble.info/) based on a [scratch docker image](https://hub.docker.com/_/scratch).

Why use this container?
----

Murmur doesn't natively support environmental variables. As such in order to use it in a container you need to bring your config file along for the ride. This container works around that by creating the murmur.ini file every time the container is started. Simply set the environmental variables below and then your off to the races.

|  Environment Variables | Default| Description |
| ---------------------------------- | ----------------|---|
| MURMUR_DATABASE | /data/mumble-server.sqlite|[wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#database) |
|MURMUR_WELCOMETEXT|<br />Welcome to this server running <b>Murmur</b>.<br />Enjoy your stay!<br /> |[wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#welcometext) |
|MURMUR_PORT|64738|[wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#port) |
|MURMUR_BANDWIDTH|72000|[wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#bandwidth) |
|MURMUR_USERS|100|[wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#users) |
|MURMUR_MESSAGEBURST| 5| |
|MURMUR_MESSAGELIMIT|1|[wiki.mumble.info]() |
|MURMUR_ALLOWPING| true|[wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#allowping) |
|MURMUR_LOGDAYS||[wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#logdays) |
|MURMUR_ALLOWHTML | | |
|MURMUR_OPUSTHRESHOLD | | |
|MURMUR_SQLITE_WAL | | |
|MURMUR_CHANNELNESTINGLIMIT | | |
|MURMUR_CHANNELCOUNTLIMIT | | |
|MURMUR_CHANNELNAME | | |
|MURMUR_USERNAME | | |
|MURMUR_IMAGEMESSAGELENGTH | | |
|MURMUR_TEXTMESSAGELENGTH | | |
|MURMUR_LOGFILE | | |
|MURMUR_AUTOBANATTEMPTS | | |
|MURMUR_AUTOBANTIMEFRAME | | |
|MURMUR_AUTOBANTIME | | |
|MURMUR_GRPC | | |
|MURMUR_GRPCCERT | | |
|MURMUR_GRPCKEY | | |
|MURMUR_ICE | | |
|MURMUR_ICESECRETREAD | | |
|MURMUR_ICESECRETWRITE | | |
|MURMUR_DBUS | |[wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#dbus) |
|MURMUR_DBUSSERVICE | | [wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#dbusservice)|
|MURMUR_DBDRIVER | | [wiki.mumble.info](https://wiki.mumble.info/wiki/Murmur.ini#dbDriver) |
|MURMUR_DBUSERNAME | | |
|MURMUR_DBPASSWORD | | |
|MURMUR_DBHOST | | |
|MURMUR_DBPORT | | |
|MURMUR_DBPREFIX | | |
|MURMUR_DBOPTS | | |
|MURMUR_REGISTERNAME | | |
|MURMUR_REGISTERPASSWORD | | |
|MURMUR_REGISTERURL | | |
|MURMUR_REGISTERHOSTNAME | | |
|MURMUR_BONJOUR | | |
|MURMUR_SSLCERT | | |
|MURMUR_SSLKEY | | |
|MURMUR_SSLDHPARAMS | | |
|MURMUR_SSLCIPHERS | | |
|MURMUR_UNAME | | |
|MURMUR_SENDVERSION | | |
|MURMUR_LEGACYPASSWORDHASH | | |
|MURMUR_KDFITERATIONS | | |


### Available on


[Quay.io](https://quay.io/repository/kristopherkram/murmur)

[Docker Hub](https://hub.docker.com/r/kristopherkram/murmur)

[Gitlab](https://gitlab.com/kristopherkram/murmur/container_registry)

`docker pull quay.io/kristopherkram/murmur -p 64738:64738/tcp -p 64738:64738/udp -v murmur-server-data:/data`


[Issues](https://gitlab.com/kristopherkram/murmur/-/issues)

[Source code](https://gitlab.com/kristopherkram/murmur)

![Docker Pulls](https://img.shields.io/docker/pulls/kristopherkram/murmur)


Example systemd unit
----
```[Unit]
Description=murmur-server
After=docker.service
Requires=docker.service
[Service]
TimeoutStartSec=0
ExecStartPre=-/usr/bin/docker kill murmur-server
ExecStartPre=-/usr/bin/docker rm murmur-server
ExecStartPre=-/usr/bin/docker volume create murmur-server-data
ExecStartPre=/usr/bin/docker pull kristopherkram/murmur:test
ExecStart=/usr/bin/docker run -e MURMUR_SSLCERT=/etc/letsencrypt/live/%H/fullchain.pem -e MURMUR_SSLKEY=/etc/letsencrypt/live/%H/privkey.pem -e MURMUR_SSLCIPHERS='ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384' -e MURMUR_PORT=64738 -e MURMUR_WELCOMETEXT="Welcome to %H" -p 64738:64738/tcp -p 64738:64738/udp -v murmur-server-data:/data -v /etc/letsencrypt:/etc/letsencrypt --name murmur-server kristopherkram/murmur:test
[Install]
WantedBy=multi-user.target
```

Technical details
---
This images uses the latest static murmur Linux server builds from the projects GitHub page. A Golang based program is then ran to generate the murmur.ini file. After that the server is started in a from scratch Docker container.
