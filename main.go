// Creates the murmur.ini file for murmur based on environment variables then launches murmur
package main

import (
	"github.com/go-ini/ini"
	"github.com/spf13/viper"
	"log"
	"os"
	"os/exec"
)

func main() {
	v := viper.New()
	v.SetEnvPrefix("MURMUR")
	v.AutomaticEnv()
	os.Mkdir("/.murmurd/", 0700)
	os.Mkdir("/data/", 0700)
	cfg := ini.Empty()
	// SQLite
	v.SetDefault("database", "/data/mumble-server.sqlite")
	cfg.Section("").Key("database").SetValue(v.GetString("database"))
	v.SetDefault("welcometext", "<br />Welcome to this server running <b>Murmur</b>.<br />Enjoy your stay!<br />")
	cfg.Section("").Key("welcometext").SetValue(v.GetString("welcometext"))
	v.SetDefault("port", "64738")
	cfg.Section("").Key("port").SetValue(v.GetString("port"))
	v.SetDefault("serverpassword", "")
	cfg.Section("").Key("serverpassword").SetValue(v.GetString("serverpassword"))
	v.SetDefault("bandwidth", "72000")
	cfg.Section("").Key("bandwidth").SetValue(v.GetString("bandwidth"))
	v.SetDefault("users", "100")
	cfg.Section("").Key("users").SetValue(v.GetString("users"))
	// rate limiting
	v.SetDefault("messageburst", "5")
	cfg.Section("").Key("messageburst").SetValue(v.GetString("messageburst"))
	v.SetDefault("messagelimit", "1")
	cfg.Section("").Key("messagelimit").SetValue(v.GetString("messagelimit"))
	// Ping
	v.SetDefault("allowping", "true")
	cfg.Section("").Key("allowping").SetValue(v.GetString("allowping"))

	keys := []string{
		"logdays",
		"allowhtml",
		"opusthreshold",
		"sqlite_wal",
		"channelnestinglimit",
		"channelcountlimit",
		"channelname",
		"username",
		"imagemessagelength",
		"textmessagelength",
		"logfile",
		"autobanAttempts",
		"autobanTimeframe",
		"autobanTime",
		"grpc",
		"grpccert",
		"grpckey",
		"ice",
		"icesecretread",
		"icesecretwrite",
		"dbus",
		"dbusservice",
		"dbDriver",
		"dbUsername",
		"dbPassword",
		"dbHost",
		"dbPort",
		"dbPrefix",
		"dbOpts",
		"registerName",
		"registerPassword",
		"registerUrl",
		"registerHostname",
		"bonjour",
		"sslCert",
		"sslKey",
		"sslDHParams",
		"sslCiphers",
		"uname",
		"certrequired",
		"sendversion",
		"legacyPasswordHash",
		"kdfIterations",
	}
	for _, key := range keys {
		if v.GetString(key) != "" {
			cfg.Section("").Key(key).SetValue(v.GetString(key))
		}
	}
	// Ice
	v.SetDefault("Ice.Warn.UnknownProperties", "5")
	cfg.Section("Ice").Key("Ice.Warn.UnknownProperties").SetValue(v.GetString("Ice.Warn.UnknownProperties"))
	v.SetDefault("Ice.MessageSizeMax", "65536")
	cfg.Section("Ice").Key("Ice.MessageSizeMax").SetValue(v.GetString("Ice.MessageSizeMax"))
    // Save the config
	cfg.SaveTo("/.murmurd/murmur.ini")
	cmd := exec.Command("/murmur.x86", "-fg")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Fatal("Failed to lauch murmur ", err)
	}
}
