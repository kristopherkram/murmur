module gitlab.com/kristopherkram/murmur

go 1.16

require (
	github.com/go-ini/ini v1.62.0
	github.com/spf13/viper v1.7.1
)
